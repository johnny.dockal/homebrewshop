// after page loads
$(document).ready(function () {
    checkAge();
});

function showDiffform(checkbox) {
    if (checkbox.checked) {
        $('#fieldset-diffform').show();
    } else {
        $('#fieldset-diffform').hide();
    }
}

function showComform(checkbox) {
    if (checkbox.checked) {
        //$('#fieldset-nameform').hide();
        $('#fieldset-companyform').show();
    } else {
        $('#fieldset-companyform').hide();
        //$('#fieldset-nameform').show();
    }
}

function dimm() {
    $("#overlay").show(0);
}

function undimm() {
    $("#overlay").hide(0);
}

function validateEmail(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else
        var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}
;

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ')
            c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0)
            return c.substring(nameEQ.length, c.length);
    }
    return null;
}
;

function checkAge() {
    if (!readCookie('pivoklub_age_verify')) {
        dimm();
        $('#overlay-age-verify').show(0);
    }
}

function confirmAge() {
    createCookie('pivoklub_age_verify', 1, 365); // expire in 1 year
    undimm();
    $('#overlay-age-verify').hide(0);
}

function close_window() {
    if (confirm("Close Window?")) {
        close();
    }
}

function closeTab() {
    window.close();
}

function addProductToCart(productId, productPrice) {
    //flyToCartAnimation(productId, fly);
    quantity = $('#quantity-' + productId).val();
    quantity = quantity.replace(",", ".");
    if (quantity.indexOf('.') > -1)
    {
        quantity = Math.ceil(quantity);
        alert("Naše homebrew produkty jsou většinou vakuově balené v uvedeném množství, je tedy možné objednávat pouze po celých kusech.");
    }
    count = parseInt($('#count-' + productId).text()) + parseInt(quantity);
    //alert('quantity: ' + quantity + ' count: ' + count);

    var req = $.ajax({
        url: '/ajax/addproduct',
        type: 'POST',
        data: {
            product_id: productId,
            product_price: productPrice,
            product_quantity: count
        }
    });

    req.done(function (data) {
        // edit product count in span
        //cnt = parseInt($('#product-sum-' + productId).text()) + parseInt(quantity);
        $('#box-' + productId).show();
        $('#count-' + productId).show();
        $('#count-' + productId).text(count);

        reloadCartInfo(data.quantity, data.price);
    });
}

function flyToCartAnimation(productId, fly) {
    cart = $('#cart');

    if (fly) {
        productCart = $('#product-sum-' + productId).parent().find('img').eq(0);

        hiddenCart = false;
        if (productCart.is(":visible")) {
            hiddenCart = true;
        }

        productCart.show(0);

        var flyingImage = productCart.clone()
                .offset({top: productCart.offset().top, left: productCart.offset().left})
                .css({'opacity': '0.8', 'position': 'absolute', 'z-index': '1001'})
                .appendTo($('body'))
                .animate({
                    'top': cart.offset().top,
                    'left': cart.offset().left
                }, 1000, 'easeInOutExpo');

        if (!hiddenCart) {
            productCart.hide(0);
        }

        flyingImage.animate({'width': 0, 'height': 0}, function () {
            $(this).detach()
        });
    }

    // shake the cart
    setTimeout(function () {
        cart.effect("shake", {
            times: 1,
            direction: "up",
            distance: 5
        }, 200);
    }, 1500);
}

function reloadCartInfo(quantity, price) {
    $('#cart-num').text(quantity);
    $('#cart-sum').text(price);

    // v pripade, ze byl prazdny kosik a tohle je prvni polozka
    $('#cart-empty').hide(0);
    $('#cart-full').show(0);
}

function confirmCaptcha(formElement) {
    // data = formElement.serialize();

    var req = $.ajax({
        url: '/ajax/emailsignin',
        type: 'POST',
        data: {
            captchaid: $('#captcha-id').val(),
            captchainput: $('#captcha-input').val(),
            address: $('#captchaaddress').val()
        }
    });

    req.done(function (data) {
        $("#ajax-form-output").html(data.message);

        dimm();
        $("#overlay-form-info").show(0);
        $("#overlay-form-captcha").hide(0);

        setTimeout(function () {
            window.location.reload();
        }, 1500);
    });
}


