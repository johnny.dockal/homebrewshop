<?php

// Define path to application directory
defined('APPLICATION_PATH') || define('APPLICATION_PATH', realpath(dirname(__FILE__) . '/../application'));

//z nějakého neznámého důvodu mi nejde na ONEBITU nastavit v .htaccess APPLICATION_ENV na cokoliv jiného než production
//takže takhle to ochcávám, aby se mi tam nahodilo "testing" v případě, že to běží na demu
$url = $_SERVER['HTTP_HOST'];
$url_array = explode('.', $url);
if (isset($url_array[0]) and ( $url_array[0] == 'test')) {
    $enviroment = 'testing';
} else if ($url_array[1] == 'local') {
    $enviroment = 'development';
} else {
    $enviroment = 'production';
}

// Define application environment
defined('APP_ENV') || define('APP_ENV', (getenv('APP_ENV') ? getenv('APP_ENV') : $enviroment));

// Define application name
defined('APP_NAME') || define('APP_NAME', 'homebrewshop');

// Define application name
defined('APP_NAME_FULL') || define('APP_NAME_FULL', 'Homebrewshop');

// Define application ID 1=balkanova.cz, 2=balkanova.de, 3=bulharskavina.cz
defined('APP_ID') || define('APP_ID', 1);

// Define application locale
defined('APP_LOCALE') || define('APP_LOCALE', 'cz');

// Define application contact address
defined('APP_EMAIL') || define('APP_EMAIL', 'eshop@pivovarskyklub.com');

// Define application contact address for testing purposes
defined('APP_TESTMAIL') || define('APP_TESTMAIL', 'jan_dockal@seznam.cz');

// Define application contact address
defined('APP_EMAIL') || define('APP_EMAIL', 'jan_dockal@seznam.cz');

// Define application contact address for testing purposes
defined('APP_TESTMAIL') || define('APP_TESTMAIL', 'jan_dockal@seznam.cz');

// Define product url (in case images are taken from external source)
defined('APP_URL') || define('APP_URL', 'http://www.homebrewshop.cz/');

// Define product url (in case images are taken from external source)
defined('APP_URL_TEST') || define('APP_URL_TEST', 'http://test.homebrewshop.cz/');

// Define product url (in case images are taken from external source)
defined('IMG_URL') || define('IMG_URL', 'http://www.pivovarskyklub.com/');

// Ensure library/ is on include_path
set_include_path(implode(PATH_SEPARATOR, array(
    realpath(APPLICATION_PATH . '/../library'),
    get_include_path(),
)));

/** Zend_Application */
require_once 'Zend/Application.php';

// Create application, bootstrap, and run
$application = new Zend_Application(
        APP_ENV, APPLICATION_PATH . '/configs/application.ini'
);

//Setup Ini Configuration, save to registry
$config = new Zend_Config_Ini(APPLICATION_PATH . '/configs/application.ini', 'staging');
Zend_Registry::set('config', $config);

$front = Zend_Controller_Front::getInstance();
//Setup Router - SEO URL
$router = $front->getRouter();
//REGEXP PRO ROUTER
//pokud url nezačíná jedním ze slov v závorce, pošleme celý řetezec v parametru url na CatalogueController
$route = new Zend_Controller_Router_Route_Regex(
        '^((?!cart|sitemap|sitemap\.xml|admin|ajax|redirect)\S+)', array(
            'module' => 'eshop',
            'controller' => 'index'
        ),
        array(1 => 'url'), 'index/%s'
);
//set seo-url router
$router->addRoute('seo-url', $route);
//info
$menu = new Zend_Controller_Router_Route(
        'info', 
        array(
            'module' => 'eshop',
            'controller' => 'index',
            'action' => 'info'
        )
);
$router->addRoute('nabidka-menu', $menu);

$application->bootstrap()
        ->run();

function dump($var) {
    echo "<div style='position: relative; z-index: 999'><pre>";
    var_dump($var);
    echo "</pre></div>";
}

function sanitizeInputInt($i) {
    $temp = str_replace(',', '.', $i);
    $temp2 = preg_replace('/[^0-9.]*/', '', $temp);
    return ceil($temp2);
}
