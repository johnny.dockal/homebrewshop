<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopCountries extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_countries';
    protected $_primary = 'country_id';
    
    public function fetchCountries() {
        $session = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('country_id', 'title_'.$session->lang.' AS title'));
        $result = $this->fetchAll($query)->toArray();
        return $result;
    }
}