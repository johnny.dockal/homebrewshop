<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_SubcatProducts extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_subcat_products';
    protected $_primary = 'relation_id';
    
    function fetchProductSubcats($product_id) {
        $subcats = $this->fetchAll('product_id = '.$product_id)->toArray();
        $subcatArray = array ();
        foreach ($subcats as $value) {
            array_push($subcatArray, $value['subcategory_id']);
        }
        return $subcatArray;
    }

}