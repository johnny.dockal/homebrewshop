<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopShipping extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_deliveries';
    protected $_primary = 'shipping_id';
    
    public function fetchShipping($country_id = null, $weight = null, $shipping_id = null) {        
        $session = new Zend_Session_Namespace('Default');
        if (isset($shipping_id)) {
            $query = $this->select()->from($this->_name, array('shipping_id', 'public', 'title_'.$session->lang.' AS title', 'text_'.$session->lang.' AS text', 'price'))->where("shipping_id = '$shipping_id'");
            $result = $this->fetchRow($query);
        } else {
            $query = $this->select()->from($this->_name, array('shipping_id', 'public', 'title_'.$session->lang.' AS title', 'text_'.$session->lang.' AS text', 'price'));
            $result = $this->fetchAll($query)->toArray();
        }
        return $result;
    }
}