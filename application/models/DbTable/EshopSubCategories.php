<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopSubCategories extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_subcategories';
    protected $_primary = 'subcategory_id';
    protected $lang = null;

    public function init() {
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;
    }

    public function fetchSubcategories($category_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getAdapter()->select()
                ->from(array('es' => $this->_name), array('subcategory_id', 'sequence', 'public', 'type', 'title' => 'title_' . $this->lang, 'alias' => 'alias_' . $this->lang, 'text' => 'text_' . $this->lang))
                ->join(array('ec' => 'eshop_categories'), 'es.category_id = ec.category_id', array('category_id', 'cat_title' => 'title_' . $this->lang, 'cat_alias' => 'alias_' . $this->lang));
        if (isset($category_id)) {
            $select->where('es.category_id = ?', $category_id);
        }
        $select->order('es.category_id');
        try {
            $result = $db->fetchAll($select);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n";
            echo "Message: " . $e->getMessage() . "\n";
            echo "\n <br/>SQL: $select \n <br/>";
        }
        return $result;
    }

    public function fetchSubCategoriesAdmin($eshop_id = null) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $sql = "SELECT * "
                . "FROM eshop_categories AS ec JOIN eshop_subcategories AS es
                        ON ec.category_id = es.category_id";
        if (isset($eshop_id) && !empty($eshop_id)) {
            $sql .= " WHERE ec.eshop_id = '$eshop_id' ORDER BY ec.eshop_id, ec.category_id, es.sequence";
        } else {
            $sql .= " ORDER BY ec.eshop_id, ec.category_id, es.sequence";
        } 
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: $select \n <br/>";
        }
        return $result;
    }
    
    public function fetchSubCategoryById($subcategory_id) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getAdapter()->select()
                ->from(array('es' => $this->_name), array('subcategory_id', 'sequence', 'public', 'type', 'title' => 'title_' . $this->lang, 'alias' => 'alias_' . $this->lang, 'text' => 'text_' . $this->lang))
                ->join(array('ec' => 'eshop_categories'), 'es.category_id = ec.category_id', array('category_id', 'cat_title' => 'title_' . $this->lang, 'cat_alias' => 'alias_' . $this->lang))
                ->where("es.subcategory_id = ?", $subcategory_id);        
        try {
            $result = $db->fetchRow($select);
            return $result;
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: $select \n <br/>";
        }
    }

    public function fetchSubCategoryByAlias($alias) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $select = $this->getAdapter()->select()
                ->from(array('es' => $this->_name), array('subcategory_id', 'sequence', 'public', 'type', 'title' => 'title_' . $this->lang, 'alias' => 'alias_' . $this->lang, 'text' => 'text_' . $this->lang))
                ->join(array('ec' => 'eshop_categories'), 'es.category_id = ec.category_id', array('category_id', 'cat_title' => 'title_' . $this->lang, 'cat_alias' => 'alias_' . $this->lang))
                ->where("es.alias_$this->lang = ?", $alias);        
        try {
            $result = $db->fetchRow($select);
            return $result;
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: $select \n <br/>";
        }
    }

    public function fetchSubcategoriesOfGroup($group_id, $ids = array()) {
        $session = new Zend_Session_Namespace('Default');
        $db = Zend_Db_Table::getDefaultAdapter();
        // parent_id znamena, ke ktere kategorii tahle subkategorie patri
        // po zmene ziskavani dat pres produkty smazat z testovaci db sloupec parent_id (v produkcni neni)
        $sql = "SELECT DISTINCT esg.group_id, esg.subcategory_id, esc.url_$session->lang AS alias, esc.title_$session->lang AS title "
                . "FROM eshop_subcat_groups AS esg "
                . "JOIN eshop_subcategories AS esc ON esc.subcategory_id = esg.subcategory_id "
                . "WHERE esg.group_id = '$group_id' ";
        $i = 0;
        foreach ($ids as $id) {
            if ($i > 0) {
                $sql .= "OR ";
            } else {
                $sql .= "AND (";
            }
            $sql .= "  esc.subcategory_id = '$id' ";
            $i++;
        }
        $sql .= ")";
        try {
            $result = $db->fetchAll($sql);
        } catch (Zend_Exception $e) {
            echo "Caught exception " . __METHOD__ . ": " . get_class($e) . "\n <br/>";
            echo "\n <br/>Message: " . $e->getMessage() . "\n <br/>";
            echo "\n <br/>SQL: " . $sql . "\n <br/>";
        }
        return $result;
    }

}
