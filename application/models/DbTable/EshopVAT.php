<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_EshopVAT extends Zend_Db_Table_Abstract {

    private static $_instance = null; // use private here
    protected $_name = 'eshop_vat';
    protected $_primary = 'vat_rate';
    private $_vat1 = null;
    private $_vat2 = null;
    
    public static function getInstance()
    {
        if (self::$_instance === null) { // use strictly equal to null
            self::$_instance = new self();
        }

        return self::$_instance;
    }
    
    public function getVAT() {
        $session = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('vat_rate', 'vat', 'title_'.$session->lang.' AS title'));
        $result = $this->fetchAll($query)->toArray();
        $this->_vat1 = $result[0]['vat'];
        $this->_vat2 = $result[1]['vat'];        
    }
    
    public function getVAT1() {
        if (empty($this->_vat1)) {
            $this->getVAT();
        }
        return $this->_vat1;
    }
    
    public function getVAT2() {
        if (empty($this->_vat2)) {
            $this->getVAT();
        }
        return $this->_vat2;
    }
}