<?php
/* 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
class Model_DbTable_Payments extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_payments';
    protected $_primary = 'payment_id';
    
    public function fetchPayments($delivery_id = null, $delivery_count = null) {
        $defaultSession = new Zend_Session_Namespace('Default');
        $query = $this->select()->from($this->_name, array('payment_id', 'public', 'title_'.$defaultSession->lang.' AS title', 'text_'.$defaultSession->lang.' AS text', 'price'));
        $result = $this->fetchAll($query)->toArray();
        //ochcávka kvůli tomu, aby se nezobrazovala dobírka u osobního odběru
        if ($delivery_id == 1) {
            unset($result[3]);
        }
        return $result;
    }
}