<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Model_DbTable_EshopProducts extends Zend_Db_Table_Abstract {

    protected $_name = 'eshop_products';
    protected $_primary = 'product_id';
    protected $_limit = 30;
    protected $_productQuery_grouped = null;
    protected $_productQuery = null;
    protected $_productQueryEdit = null;
    private $columns = null;
    private $lang = null;

    public function init() {
        $session = new Zend_Session_Namespace('Default');
        $this->lang = $session->lang;
        $this->locale = $session->locale;
        $this->columns = array('id' => 'product_id', 'status' => "status_$this->locale", 'alias' => "alias_$this->lang", 'title' => "title_$this->lang", 'text' => "text_$this->lang", 'producer', 'size', 'price' => "price_$this->locale", 'vat_rate');
    }

    public function fetchProductById($product_id) {
        $select = $this->getAdapter()->select()
                ->from(array('EP' => 'eshop_products'), $this->columns)
                ->where("EP.product_id = ?", $product_id);
        try {
            $result = $this->getAdapter()->fetchRow($select);
            return $result;
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $select . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
        }
    }
    
    public function fetchProductByAlias($alias) {
        $db = Zend_Db_Table::getDefaultAdapter();
        try {
            $select = "SELECT     ep.product_id, 
                                    ep.status_$this->locale AS status,
                                    ep.vat_rate,    
                                    GROUP_CONCAT(es.category_id SEPARATOR ';') AS category_id,
                                    GROUP_CONCAT(es.subcategory_id SEPARATOR ';') AS subcategory_id,
                                    GROUP_CONCAT(es.title_cz SEPARATOR ';') AS subcategory_title,
                                    es.type AS type,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,                                    
                                    ep.price_$this->locale AS price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text
                        FROM eshop_products AS ep JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) 
                            WHERE ep.alias_$this->lang = '$alias' GROUP BY ep.product_id ";
            $product = $db->fetchRow($select);
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $select . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
        }
        //Pokud napoprvé není žádný výsledek, může se jednat o produkt, který není zařazen do žádné subkategorie
        if (empty($product)) {
            try {
                $select = "SELECT     ep.product_id, 
                                    ep.status_$this->locale AS status,
                                    ep.vat_rate,  
                                    es.subcategory_id,
                                    es.title_cz AS subcategory_title,
                                    es.type,
                                    ep.title_cz AS title,
                                    ep.text_cz AS text,
                                    ep.producer,
                                    ep.size,                                    
                                    ep.price_$this->locale AS price,
                                    ec.category_id,
                                    ec.title_cz AS category_title,
                                    ec.text_cz AS category_text
                        FROM eshop_products AS ep LEFT JOIN (eshop_subcat_products AS esp, eshop_subcategories AS es, eshop_categories AS ec) 
                            ON (esp.product_id = ep.product_id AND es.subcategory_id = esp.subcategory_id AND ec.category_id = es.category_id) 
                            WHERE ep.alias_$this->lang = '$alias'";
                $product = $db->fetchRow($select);
            } catch (Zend_Exception $e) {
                echo "Caught exception at: " . __METHOD__ . "<br/>";
                echo "Exception: " . get_class($e) . "<br/>";
                echo "Message: " . $e->getMessage() . "<br/>";
                echo "SQL: " . $select . "<br/>";
                Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
            }
        }
        return $product;
    }

    public function fetchProductsSearch($search) {
        $db = Zend_Db_Table::getDefaultAdapter();
        $products = array();
        //$keyword = "%".preg_replace(' ', '[%]', $search)."%";
        $sql = $this->_productQuery . "WHERE c.eshop_id = " . APP_ID . " AND LOWER (p.title_cz) LIKE LOWER('%$search%') AND (status != '0') AND (status != '3') GROUP BY p.product_id LIMIT 30";
        //pošlu query
        try {
            $result = $db->fetchAll($sql);
            foreach ($result as $data) {
                $products[$data['product_id']] = new Model_EshopProduct($data);
            }
            return $products;
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $sql . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $sql);
        }
    }

    public function fetchProductsByIds($idArray) {
        $select = $this->getAdapter()->select()
                ->from(array('EP' => 'eshop_products'), $this->columns)
                ->where('product_id in ( ? )', $idArray);
        try {
            $result = $this->getAdapter()->fetchAll($select);
            return $result;
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $select . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
        }
    }

    public function fetchProductsByCat($category_id, $limit = null, $page = null, $order = 'title') {
        $select = $this->getAdapter()->select()
                ->from(array('EP' => 'eshop_products'), $this->columns)
                ->join(array('ESP' => 'eshop_subcat_products'), 'EP.product_id = ESP.product_id')
                ->join(array('ES' => 'eshop_subcategories'), 'ESP.subcategory_id = ES.subcategory_id')
                ->where('EP.status_cz > 0')
                ->where('ES.category_id = ?', $category_id)
                ->group('EP.product_id');
        if ($order) {
            $select->order($order);
        }
        if ($limit && $page) {
            $select->limitPage($page, $limit);
        }
        $select = substr_replace($select, 'SELECT SQL_CALC_FOUND_ROWS', 0, 6);
        try {
            $products = $this->getAdapter()->fetchAll($select);
            $count = $this->getAdapter()->fetchOne(new Zend_Db_Expr('SELECT FOUND_ROWS()'));
            return array('products' => $products, 'count' => $count);
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $select . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
        }
    }

    public function fetchProductsBySubcat($subcategory_id, $limit = null, $page = null, $order = 'title') {
        $select = $this->getAdapter()->select()
                ->from(array('EP' => 'eshop_products'), $this->columns)
                ->join(array('ESP' => 'eshop_subcat_products'), 'EP.product_id = ESP.product_id')
                ->where('EP.status_cz > 0')
                ->where('ESP.subcategory_id = ?', $subcategory_id);
        if ($order) {
            $select->order($order);
        }
        if ($limit && $page) {
            $select->limitPage($page, $limit);
        }
        $select = substr_replace($select, 'SELECT SQL_CALC_FOUND_ROWS', 0, 6);
        try {
            $products = $this->getAdapter()->fetchAll($select);
            $count = $this->getAdapter()->fetchOne(new Zend_Db_Expr('SELECT FOUND_ROWS()'));
            return array('products' => $products, 'count' => $count);
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $select . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
        }
    }

    public function fetchProductsByGroup($group_id, $limit = null, $page = null, $order = 'title') {
        $select = $this->getAdapter()->select()
                ->from(array('ESG' => 'eshop_subcat_groups'))
                ->join(array('ES' => 'eshop_subcategories'), 'ESG.subcategory_id = ES.subcategory_id')
                ->join(array('ESP' => 'eshop_subcat_products'), 'ESP.subcategory_id = ES.subcategory_id')
                ->join(array('EP' => 'eshop_products'), 'EP.product_id = ESP.product_id', $this->columns)
                ->where('EP.status_cz > 0')
                ->where('ESG.group_id = ?', $group_id);
        if ($order) {
            $select->order($order);
        }
        if ($limit && $page) {
            $select->limitPage($page, $limit);
        }
        $sSelect = substr_replace($select, 'SELECT SQL_CALC_FOUND_ROWS', 0, 6);
        try {
            $products = $this->getAdapter()->fetchAll($sSelect);
            $count = $this->getAdapter()->fetchOne(new Zend_Db_Expr('SELECT FOUND_ROWS()'));
            return array('products' => $products, 'count' => $count);
        } catch (Zend_Exception $e) {
            echo "Caught exception at: " . __METHOD__ . "<br/>";
            echo "Exception: " . get_class($e) . "<br/>";
            echo "Message: " . $e->getMessage() . "<br/>";
            echo "SQL: " . $select . "<br/>";
            Model_DbTable_ErrorLog::getInstance()->log($_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], __METHOD__, get_class($e), $e->getMessage(), $select);
        }
    }

}
