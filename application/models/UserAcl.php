<?php

/**
 * Vytvoření uživatelských rolí guests a admin.
 * Nastavení jejich oprávnění, tj. na jaké stránky mají přístup.
 * @author Daniel Vála
 */
class Model_UserAcl extends Zend_Acl {

    function __construct() {
        // Roles
        $this->addRole(new Zend_Acl_Role('guest'));
        $this->addRole(new Zend_Acl_Role('editor'), 'guest');
        $this->addRole(new Zend_Acl_Role('admin'), 'editor');
        $this->addRole(new Zend_Acl_Role('superadmin'), 'admin');
        
        // Resources
        
        // Eshop module
        $this->add(new Zend_Acl_Resource('eshop'))  
                ->add(new Zend_Acl_Resource('eshop:ajax', 'eshop')) 
                ->add(new Zend_Acl_Resource('eshop:index', 'eshop'))
                ->add(new Zend_Acl_Resource('eshop:list', 'eshop'))
                ->add(new Zend_Acl_Resource('eshop:cart', 'eshop'))
                ->add(new Zend_Acl_Resource('eshop:product', 'eshop'))
                ->add(new Zend_Acl_Resource('eshop:error', 'eshop'));         

        // Permission
        // Guests mohou jen na defaultní modul.      
        $this->allow('guest', 'eshop:ajax');           
        $this->allow('guest', 'eshop:index'); 
        $this->allow('guest', 'eshop:list'); 
        $this->allow('guest', 'eshop:product'); 
        $this->allow('guest', 'eshop:cart');
        
    }

}
