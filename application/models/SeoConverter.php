<?php

class Model_SeoConverter {

    function makeSeo($url, $id = null, $limit = 75) {
        $url = preg_replace('~[^\\pL0-9_]+~u', '-', $url);
        $url = trim($url, "-");
        $url = iconv("utf-8", "us-ascii//TRANSLIT", $url);
        $url = strtolower($url);
        $url = preg_replace('~[^-a-z0-9_]+~', '', $url);

        if (isset($id)) {
            $url = "$id-" . $url;
        }

        if (strlen($url) > 70) {
            $url = substr($url, 0, 70);
        }

        if (empty($url)) {
            //return 'n-a';
            return time();
        }

        return $url;
    }

}
