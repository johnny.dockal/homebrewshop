<?php

/**
 * Formulář k přihlášení se do administrace.
 *
 * @package default
 * @author Daniel Vála
 */
class Form_Settingsform extends Zend_Form {

    public function __construct() {
        parent::__construct();
        $this->setMethod('POST')->setName('login')->setAction('/eshop/settings/save/');
        $this->setAttrib('class', 'orderform');
        
        $view = Zend_Layout::getMvcInstance()->getView();        
       
        $order_name = new Zend_Dojo_Form_Element_TextBox('order_name', array('class' => 'textbox'));
        $order_name->setLabel('Číslo účtu')->setRequired(true);
        
        $order_surname = new Zend_Dojo_Form_Element_TextBox('order_surname', array('class' => 'textbox'));
        $order_surname->setLabel($view->form_surname)->setRequired(true);

        $order_phone = new Zend_Dojo_Form_Element_TextBox('order_phone', array('class' => "textbox", 'value' => '+420'));
        $order_phone->setLabel($view->form_phone)->setRequired(true);
        
        $order_email = new Zend_Dojo_Form_Element_TextBox('order_email', array('class' => 'textbox', 'value' => '@'));
        $order_email->setLabel($view->form_email)->setRequired(true);

        $order_address = new Zend_Dojo_Form_Element_TextBox('$order_address', array('class' => "textbox"));
        $order_address->setLabel($view->form_address)->setRequired(true);
        
        $order_city = new Zend_Dojo_Form_Element_TextBox('$order_city', array('class' => "textbox"));
        $order_city->setLabel($view->form_city)->setRequired(true);
        
        $order_zip = new Zend_Dojo_Form_Element_TextBox('$order_zip', array('class' => "textbox"));
        $order_zip->setLabel($view->form_zip)->setRequired(true);
        
        $country_id = new Zend_Form_Element_Select('country_id', array('class' => 'textbox'));
        $country_id->setLabel($view->form_country)->setRequired(true);

        $submit = new Zend_Form_Element_Submit('submit', array('label' => "Pokračovat", 'class' => "button bg-green border-style active"));

        $this->addElements(array(
            $order_name, $order_surname, $order_phone, $order_email, $order_address, $order_city, $order_zip, $country_id, $submit
        ));
    }
}
