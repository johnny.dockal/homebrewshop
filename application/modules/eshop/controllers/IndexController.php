<?php

class Eshop_IndexController extends Zend_Controller_Action {

    private $defaultSession = null;
    private $cartSession = null;
    private $page = 1;
    private $limit = 60;

    public function init() {
        /* Initialize action controller here */
    }

    public function preDispatch() {
        $this->cartSession = new Zend_Session_Namespace('Cart');
        $this->defaultSession = new Zend_Session_Namespace('Default');
    }

    public function indexAction() {
        $page = $this->getParam('page');
        if (!empty($page)) {
            $this->page = $page;
        }
        $this->view->page = $this->page;
        $uri = Zend_Controller_Front::getInstance()->getRequest()->getRequestUri();
        //Homepage
        if  (($uri == "/") || ($uri == "/eshop/")) {
            $subcatmodel = new Model_DbTable_EshopSubCategories();
            $this->view->subcategories = array(
                '1' => $subcatmodel->fetchSubcategories('1'),
                '2' => $subcatmodel->fetchSubcategories('2'),
                '3' => $subcatmodel->fetchSubcategories('3'));
            return;
        }
        //Other pages
        if (strpos($uri, '?') !== false) {
            $uri = substr($uri, 0, strpos($uri, "?"));
        }
        $urls = explode("/", $uri);
        if (isset($urls[2]) && (($urls[2] == 'group') || ($urls[2] == 'subcategory'))) {
            $alias = $urls[1];
            $id = $urls[3];
            $mCategory = new Model_DbTable_EshopCategories();
            $mGroups = new Model_DbTable_EshopGroups();
            $mProduct = new Model_DbTable_EshopProducts();
            $category = $mCategory->fetchCategoryByAlias($alias);
            $this->limit = $this->getCategoryLimit($category['category_id']);
            if ($urls[2] == 'group') {
                $products = $mProduct->fetchProductsByGroup($id, $this->limit, $this->page);
            }
            if ($urls[2] == 'subcategory') {
                $products = $mProduct->fetchProductsBySubcat($id, $this->limit, $this->page);
            }            
            $this->view->groups = $mGroups->fetchGroupedSubcategories($category['category_id']);
            $this->view->itemcount = $products['count'];
            $this->view->pagecount = ceil($products['count'] / $this->limit);
            $this->view->products = $products['products'];
            $this->view->title = $category['title'];
            $this->view->itemlimit = $this->limit;
            $this->view->alias = $alias;
            $this->renderCategory($category['category_id']);
            return;
        } else if (isset($urls[3]) && (!empty($urls[3]))) {
            //Alias musí být produkt  
            $alias = $urls[3];
            $mProduct = new Model_DbTable_EshopProducts();
            $product = $mProduct->fetchProductByAlias($alias);
            $this->view->title = $product['title'];
            $this->view->product = $product;
            $this->render('product');
            return;
        } else if (isset($urls[2]) && (!empty($urls[2]))) {
            //Alias může být produkt nebo subkategorie
            $alias = $urls[2];
            //Zkusíme, zda alias není produkt
            $mProduct = new Model_DbTable_EshopProducts();
            $product = $mProduct->fetchProductByAlias($alias);
            if (!empty($product)) {
                $this->view->title = $product['title'];
                $this->view->product = $product;
                $this->render('product');
                return;
            }
            //Zkusíme, zda alias není subkategorie            
            $mSubcat = new Model_DbTable_EshopSubCategories();
            $subcategory = $mSubcat->fetchSubCategoryByAlias($alias);
            if (!empty($subcategory)) {
                $subcatModel = new Model_DbTable_EshopSubCategories();
                $subcategory = $subcatModel->fetchSubCategoryByAlias($alias);
                $this->limit = $this->getCategoryLimit($subcategory['category_id']);
                $products = $mProduct->fetchProductsBySubcat($subcategory['subcategory_id'], $this->limit, $this->page);
                $this->view->itemcount = $products['count'];
                $this->view->pagecount = ceil($products['count'] / $this->limit);
                $this->view->products = $products['products'];
                $this->view->title = $subcategory['title'];
                $this->view->itemlimit = $this->limit;
                $this->view->alias = $alias;
                $this->view->page = $this->page;
                $this->renderCategory($subcategory['category_id']);
                return;
            }
        } else if (isset($urls[1]) && (!empty($urls[1]))) {
            $alias = $urls[1];
            $mCategory = new Model_DbTable_EshopCategories();
            $mGroups = new Model_DbTable_EshopGroups();
            $mProduct = new Model_DbTable_EshopProducts();
            //Zkusíme, zda alias není kategorie
            $category = $mCategory->fetchCategoryByAlias($alias);
            if (!empty($category)) {
                $this->limit = $this->getCategoryLimit($category['category_id']);
                $order = ($category['category_id'] == 3) ? 'id' : 'title';
                $products = $mProduct->fetchProductsByCat($category['category_id'], $this->limit, $this->page, $order);
                $this->view->groups = $mGroups->fetchGroupedSubcategories($category['category_id']);
                $this->view->itemcount = $products['count'];
                $this->view->pagecount = ceil($products['count'] / $this->limit);
                $this->view->products = $products['products'];
                $this->view->title = $category['title'];
                $this->view->itemlimit = $this->limit;
                $this->view->alias = $alias;
                $this->renderCategory($category['category_id']);
                return;
            }
            //Zkusíme, zda alias není produkt
            $product = $mProduct->fetchProductByAlias($alias);
            if (!empty($product)) {
                $this->view->title = $product['title'];
                $this->view->product = $product;
                $this->render('product');
                return;
            }
            //Zkusíme, zda alias není subkategorie (neměla by, ale v error logu se o to občas pokouší)           
            $mSubcat = new Model_DbTable_EshopSubCategories();
            $subcategory = $mSubcat->fetchSubCategoryByAlias($alias);
            if (!empty($subcategory)) {
                $subcatModel = new Model_DbTable_EshopSubCategories();
                $subcategory = $subcatModel->fetchSubCategoryByAlias($alias);
                $this->limit = $this->getCategoryLimit($subcategory['category_id']);
                $products = $mProduct->fetchProductsBySubcat($subcategory['subcategory_id'], $this->limit, $this->page);
                $this->view->itemcount = $products['count'];
                $this->view->pagecount = ceil($products['count'] / $this->limit);
                $this->view->products = $products['products'];
                $this->view->title = $subcategory['title'];
                $this->view->itemlimit = $this->limit;
                $this->view->alias = $alias;
                $this->view->page = $this->page;
                $this->renderCategory($subcategory['category_id']);
                return;
            }
        }
        /* LEGACY */
        if  ($uri == "/eshop/cart/") {
            $this->redirect("/cart/");
        }
        $product_id     = $this->getParam('product_id'); 
        $subcategory_id = $this->getParam('subcategory_id'); 
        $category_id    = $this->getParam('category_id');
        $group_id       = $this->getParam('group_id');
        $url            = '';   
        if (!empty($product_id)) {   
            $mProduct = new Model_DbTable_EshopProducts();
            $product = $mProduct->fetchProductById($product_id);
            $alias = $product['alias'];
            $url = "/$alias";
        }
        if (!empty($subcategory_id)) {   
            $mSubcat = new Model_DbTable_EshopSubCategories();
            $subcat = $mSubcat->fetchSubCategoryById($subcategory_id);
            $alias = $subcat['alias'];
            $url = "/$alias" . $url;
        }
        if (!empty($category_id)) {   
            switch ($category_id) {
                case 1:
                    $url = "/pivo" . $url;
                    break;
                case 2:
                    $url = "/breweriana" . $url;
                    break;
                case 3:
                    $url = "/homebrew" . $url;
                    break;
            }
            
        }
        if (!empty($url)) {            
            $this->redirect($url."/");
        }
        
        throw new Zend_Controller_Action_Exception('Stránka neexistuje', 404);
    }

    public function infoAction() {
        $texts = new Model_DbTable_EshopTexts();
        $terms = $texts->find('podminky')->toArray();
        $jaknakupovat = $texts->find('jak_nakupovat')->toArray();
        $this->view->terms = $terms[0];
        $this->view->jaknakupovat = $jaknakupovat[0];
    }

    private function renderCategory($category_id) {
        switch ($category_id):
            case 1:
                $this->view->showflags2 = true;
                $this->view->size = "objem";
                $this->view->notice = true;
                $this->render('beer');
                break;
            case 2:
                $this->view->size = "velikost";
                $this->render('beer');
                break;
            case 3:
                $this->view->showflags1 = true;
                $this->render('homebrew');
                break;
        endswitch;
    }

    private function getCategoryLimit($category_id) {
        switch ($category_id):
            case 1:
            case 2:
                $limit = 60;
                break;
            default:
                $limit = 30;
        endswitch;
        return $limit;
    }

}
