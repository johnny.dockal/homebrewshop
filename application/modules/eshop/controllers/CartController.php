<?php

class Eshop_CartController extends Zend_Controller_Action {

    private $step = null;
    private $order = null;
    private $accepted = false;
    private $defaultSession = null;
    private $cartSession = null;
    private $deliveryCount = null;

    public function init() {
        $this->cartSession = new Zend_Session_Namespace('Cart');
        $this->defaultSession = new Zend_Session_Namespace('Default');
        $this->order = new Model_EshopOrder();
        $this->msg_available = array();
    }

    public function preDispatch() {
        //nastavení jednotlivých kroků
        $this->step = $this->_getParam('step', 0);
        //if ($this->getRequest()->isPost()) {
            //pokud chytíme parametr zpět, chceme se vrátit o jeden krok
            $back = $this->_getParam('cartbuttonprev');
            //pokud chytíme parametr update, tak se editují počty zboží v košíku
            $count = $this->_getParam('update');
            //zjistíme, zda zákazník odklikl základní podmínky
            $this->accepted = $this->_getParam('accepted');

            if ($count) {
                $i = 1;
                while ($i <= $count) {
                    $product_id = $this->_getParam('product_id-' . $i);
                    $quantity = $this->_getParam('quantity-' . $i);
                    $this->order->updateProduct($product_id, $quantity);
                    $i++;
                }
                $this->defaultSession->sum = $count;
            }

            if ($back) {
                $this->step = $this->step - 2;
                if ($this->step < 1) {
                    $this->_redirect('/cart/');
                }
            }

            switch ($this->step) {
                case 2:
                case 3:
                    $deleteZero = true;
                    break;
                case 1:
                default:
                    $deleteZero = false;
                    break;
            }
            $this->order->fetchCartProducts($deleteZero);
        //}


        $texts = new Model_DbTable_EshopTexts();
        $text = $texts->find('kosik_mnozstvi')->toArray();
        $this->view->amount = $text[0]['text_cz'];

        if ($this->accepted) {
            $this->defaultSession->accepted = true;
        } else {
            if ($this->defaultSession->accepted) {
                
            } else {
                $text = $texts->find('jak_nakupovat')->toArray();
                $this->view->howtoshop = $text[0];
            }
        }
    }

    public function postDispatch() {
        $this->view->order = $this->order;
        $this->view->step = $this->step;
        $this->view->size = 'velikost';
        $this->view->headTitle()->prepend('Košík'); 
    }

    public function indexAction() {
        $model = new Model_DbTable_EshopProducts();
        $idArray = array('668', '669');
        $this->view->packages = $model->fetchProductsByIds($idArray);
    }

    //Přidáme do košíku výrobek podle ID, přesměrujeme na původní stránku
    public function addAction() {
        $product_id = $this->_getParam('product_id');
        $quantity = sanitizeInputInt($this->_getParam('quantity'));
        $redir = $this->_getParam('redirect');
        //$line       = $this->_getParam('line');    
        $size = $this->_getParam('size');

        $this->order->addProduct($product_id, $quantity);
        $this->_redirect("/$redir#$line");
    }

    public function emptyAction() {
        $product_id = $this->_getParam('product_id');

        if ($product_id == 'all') {
            Zend_Session::namespaceUnset('Cart');
            $this->defaultSession->sum = 0;
        } else {
            $this->defaultSession->sum--;
            unset($this->cartSession->$product_id);
        }
        $this->_redirect("/cart/");
    }

    //Vyplnění objednávky
    public function orderAction() {
        //parametry odeslané z kroku 2, 3 a 4 při pohybu "zpět"       
        $this->order->setFormParameters($this->getRequest()->getPost());
        $this->step = $this->order->checkFormParameters($this->step);
        $this->com = $this->getParam('com', false);
        $paramArray = $this->order->formParametersToArray();

        switch ($this->step) {
            //KROK 1 - Výběr dopravy a platby
            case '1':
                $model = new Model_DbTable_EshopCountries();
                if ($this->com) {
                    $form = new Form_OrderStep1Comform($model->fetchCountries());
                } else {
                    $form = new Form_OrderStep1form($model->fetchCountries());
                }
                $form->populate($paramArray);
                //musíme zobrazit podmínky
                $texts = new Model_DbTable_EshopTexts();
                $terms = $texts->find('podminky')->toArray();
                $this->view->com = $this->com;
                $this->view->terms = $terms[0];
                $this->view->form = $form;
                break;
            //KROK 2 - Doručovací údaje
            case '2':
                $model = new Model_DbTable_EshopShipping();
                $form = new Form_OrderStep2form($model->fetchShipping(), $this->deliveryCount);
                $form->populate($paramArray);
                //musíme zobrazit text
                $texts = new Model_DbTable_EshopTexts();
                $text = $texts->find('kosik_pozn1')->toArray();
                $this->view->notice1 = $text[0]['text_cz'];
                $this->view->noedit = true;
                $this->view->form = $form;
                break;
            //KROK 3 - Platební údaje
            case '3':
                $model = new Model_DbTable_EshopPayments();
                $form = new Form_OrderStep3form($model->fetchPayments($this->order->getShippingId()), $this->deliveryCount, $this->order->getOrderTotal());
                $form->populate($paramArray);
                $this->view->noedit = true;
                $this->view->form = $form;
                break;
            //KROK 4 - Kontrola
            case '4':
                $this->view->formParameters = $paramArray;
                $this->view->noedit = true;
                $this->_helper->viewRenderer('overview');
                break;
        }
    }

    public function sendAction() {
        $view = Zend_Layout::getMvcInstance()->getView();
        $this->order->setFormParameters($this->getRequest()->getPost());
        $this->step = $this->order->checkFormParameters(4);
        $formParameters = $this->order->formParametersToArray();
        $failmessage = "";
        if (APP_ENV == 'production') {
            $debug = false;
        } else {
            $debug = true;
        }

        if ($this->step == 4) {
            $this->order->saveOrder();
        } else {
            $failmessage .= "<h2>Chyba!</h2><p>Zdá se, že vypršela úschova vašich osobních údajů. Údaje z kontaktního formuláře uchováváme jen omezenou dobu 12 minut. "
                    . "Pokud jste po tuto dobu na eshopu nečinní, tak se automaticky smažou.</p>";
        }
        $token = $this->order->getToken();
        if (!empty($token)) {
            //Připravím link na objednávku
            if ($debug) {
                $link = "$view->str_order_check <a href='" . APP_URL_TEST . "/cart/payment?token=$token'>[$view->str_order_link]</a><br/><br/>";
            } else {
                $link = "$view->str_order_check <a href='" . APP_URL . "/cart/payment?token=$token'>[$view->str_order_link]</a><br/><br/>";
            }
            //tabulka se jménem, adresou atd...
            $clientTable = "<h2>$view->form_billing</h2>"
                    . "<table width='560'>";
            if ($this->order->getCompany()) {
                $clientTable .= "<tr><td>$view->str_company</td><td>" . $this->order->getCompanyName() . "</td><tr>"
                    . "<tr><td>$view->str_ico</td><td>" . $this->order->getCompanyId() . "</td><tr>"; 
            } 
            $clientTable .= "<tr><td>$view->form_name</td><td>" . $this->order->getFullName() . "</td><tr>"  
                    . "<tr><td>$view->form_address</td><td>" . $this->order->getAddress() . "</td><tr>"  
                    . "<tr><td>$view->form_city</td><td>" . $this->order->getCity() . " " . $this->order->getZip() . "</td><tr>"
                    . "<tr><td>$view->form_country</td><td>" . $this->order->getCountryName() . "</td><tr>";
            if ($this->order->getDiff()) {
                $clientTable .= "<tr><td colspan='2'><strong>$view->str_delivery_data</strong></td></tr>"
                    . "<tr><td>$view->form_name</td><td>" . $this->order->getDiffFullName() . "</td><tr>"  
                    . "<tr><td>$view->form_address</td><td>" . $this->order->getDiffAddress() . "</td><tr>"  
                        . "<tr><td>$view->form_city</td><td>" . $this->order->getDiffCity() . " " . $this->order->getDiffZip() . "</td><tr>";
            } 
            $clientTable .= "<tr><td>$view->form_phone</td><td>" . $this->order->getPhone() . "</td><tr>"
                    . "<tr><td>$view->form_email</td><td>" . $this->order->getEmail() . "</td><tr>"
                    . "<tr><td>$view->form_delivery</td><td>" . $this->order->getShippingTitle() . "</td><tr>"
                    . "<tr><td>$view->form_payment</td><td>" . $this->order->getPaymentTitle() . "</td><tr>"
                    . "<tr><td>$view->str_message</td><td>" . $this->order->getMessage() . "</td><tr>"
                    . "</table><br/>";
            //tabulka s objednanými věcmi
            $productTable = "<br/><table width='560'><tr><td>ID</td><td>produkt</td><td>množství</td><td>cena za kus</td><td>cena celkem</td>";
            $totalPrice = 0;
            foreach ($this->order->getCartProducts() as $product) {
                $productTable .= "<tr><td>" . $product->getProductId() . "</td><td>" . $product->getTitle() . "</td><td align='right'>" . $product->getQuantity() . "</td><td align='right'>" . $product->getPrice() . ",-kč</td><td align='right'>" . $product->getTotalPrice() . ",-kč</td></tr>";
                $totalPrice += $product->getTotalPrice();
            }
            $packingCount = $this->order->getPackingCount();
            if (!empty($packingCount)) {
                $productTable .= "<tr><td></td>"
                        . "<td>Igelitová taška</td><td align='right'>" . $this->order->getPackingCount() . "</td>"
                        . "<td align='right'>" . $this->order->getPackingPrice() . ",-kč</td>"
                        . "<td align='right'>" . $this->order->getPackingTotal() . ",-kč</td></tr>";
                $totalPrice += $this->order->getPackingTotal();
            }
            $deliveryPrice = $this->order->getDeliveryPrice();
            if (!empty($deliveryPrice)) {
                $productTable .= "<tr><td></td>"
                        . "<td>Doprava</td><td align='right'>" . $this->order->getDeliveryCount() . "</td>"
                        . "<td align='right'>" . $this->order->getDeliveryPrice() . ",-kč</td>"
                        . "<td align='right'>" . $this->order->getDeliveryTotal() . ",-kč</td></tr>";
                $totalPrice += $this->order->getDeliveryTotal();
            }
            $paymentPrice = $this->order->getPaymentPrice();
            if (!empty($paymentPrice)) {
                $productTable .= "<tr><td></td><td>" . $this->order->getPaymentTitle() . "</td>"
                        . "<td align='right'>" . $this->order->getPaymentCount() . "</td>"
                        . "<td align='right'>" . $this->order->getPaymentPrice() . ",-kč </td>"
                        . "<td align='right'>" . $this->order->getPaymentTotal() . ",-kč</td></tr>";
                $totalPrice += $this->order->getPaymentTotal();
            }
            $productTable .= "<tr><td></td><td>Celkem</td><td></td><td></td><td align='right'>" . $totalPrice . ",-kč</td></tr>";
            $productTable .= "</table><br/>";
            $productTable .= "<br/>Všechny ceny jsou uvedeny včetně DPH.";
            //Seženeme si text mailu pro zákazníka
            $texts = new Model_DbTable_EshopTexts();
            $text_id = ($formParameters['shipping_id'] == 1) ? 'email_osobne' : 'email_ppl';
            $text = $texts->find($text_id)->toArray();
            //Začneme stavět mail pro zákazníka
            $mail = new Zend_Mail('utf-8');
            $mail->setFrom('eshop@pivovarskyklub.com', 'Pivovarský klub E-shop');
            $mail->addTo($formParameters['order_email']);
            $mail->setSubject("Potvrzení objednávky " . date('d. n. Y'));
            $mail->setBodyHtml($text[0]['text_cz'] . "<br/><br/>Stav objednávky můžete zkontrolovat zde: $link $clientTable $productTable");
            try {
                $mail->send();
            } catch (Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
            $mail->clearRecipients();
            $mail->clearSubject();
            $mail->clearFrom();
            //Začnem stavět mail pro provozovatele
            if ($debug) {
                $mail->setFrom($formParameters['order_email'], 'TEST Pivoklub E-shop - ' . $formParameters['order_name'] . " " . $formParameters['order_surname']);
                $mail->addTo('jan_dockal@seznam.cz');
                $mail->setSubject("TESTOVACÍ objednávka od " . $formParameters['order_name'] . " " . $formParameters['order_surname']);
            } else {
                $mail->setFrom($formParameters['order_email'], 'Pivoklub E-shop - ' . $formParameters['order_name'] . " " . $formParameters['order_surname']);
                $mail->addTo('eshop@pivovarskyklub.com');
                $mail->setSubject("Nová objednávka od " . $formParameters['order_name'] . " " . $formParameters['order_surname']);
            }
            $mail->setBodyHtml("$clientTable $link $productTable");
            //$mail->setBodyHtml("$clientTable $link $productTable <h2>Text odeslán zákazníkovi</h2> " . $text[0]['text_cz']);
            try {
                $mail->send();
            } catch (Exception $e) {
                echo "Caught exception: " . get_class($e) . "\n";
                echo "Message: " . $e->getMessage() . "\n";
            }
            $this->destroySession();
            $this->_redirect("/cart/payment/?token=$token");
        } else {
            $failmessage .= "<p>Objednávku nelze uložit</p><p>token: $token</p>";
        }
        $this->view->failmessage = $failmessage;
    }

    public function paymentAction() {
        $token = $this->getParam('token');
        if (!empty($token)) {
            $this->order->fetchOrderByToken($token);            
            switch ($this->order->getPaymentId()) {
                //1 = paypal
                case 1:
                    break;
                //2 = kreditka (CS)
                case 2:
                    break;
                //3 = převod
                case 3:
                    $settings = new Model_DbTable_Settings();
                    $this->view->account = $settings->getAccountCZ();
                    $this->view->invoice_number = $this->order->getInvoiceNumber();
                    break;
                //4 = dobírka
                case 4:
                    break;
                default:
                    break;
            }

            $this->view->noedit = true;
            $this->view->noimage = true;
        } else {
            
        }
    }

    public function obchodnipodminkyAction() {
        $texts = new Model_DbTable_EshopTexts();
        $terms = $texts->find('podminky')->toArray();
        $this->view->terms = $terms[0];
    }

    private function destroySession() {
        Zend_Session::namespaceUnset('Order');
        Zend_Session::namespaceUnset('Cart');
        $defaultSession = new Zend_Session_Namespace('Default');
        $defaultSession->sum = 0;
    }

}
