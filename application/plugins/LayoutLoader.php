<?php
/**
 * Plugin načte layout pro aktuální modul.
 * @author Daniel Vála
 */
class Plugin_LayoutLoader extends Zend_Controller_Action_Helper_Abstract {

    public function init() {
        $module = $this->getRequest()->getModuleName();
        Zend_Layout::getMvcInstance()->setLayout($module);
    }
}