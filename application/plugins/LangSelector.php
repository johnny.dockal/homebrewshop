<?php

/**
 * Plugin zjištuje požadavky na změnu jazyka webu v parametru.
 * Pokud odpovídá českému nebo anglickému jazyku, nastaví ji do Zend session.
 * @author Daniel Vála
 */
class Plugin_LangSelector extends Zend_Controller_Action_Helper_Abstract {

    private $aLanguages = array('cz');
    private $aErrormessages = array();
    private $session = null;

    public function init() {
        $this->session = new Zend_Session_Namespace('Default');
        // pokud ještě není nastaven žádný jazyk, nastaví se defaultně prvně uvedený jazyk v configu
        $module = $this->getRequest()->getModuleName();
        if ($module == 'admin') {
            $this->session->lang = 'cz';
        } else if (isset($this->session->lang)) {
            //nic
        } else if (APP_ID == 2) {
            $this->session->lang = 'de';
        } else {
            $this->session->lang = 'cz';
        }
        // pokud je zjištěn požadavek na změnu jazyka, kontroluje se, zda je jazyk v požadavku jeden z uvedených v konfigu a nastaví se
        // pokud nevyhovuje nebo není vůbec nastaven, zachová se stávající jazyk
        $lang = $this->getRequest()->getParam('lang');
        if (isset($lang)) {
            if (in_array($lang, $this->aLanguages)) {
                $this->session->lang = $lang;
            } else {
                array_push($this->aErrormessages, "Chyba v požadavku na změnu jazyka, jazyk nenalezen v config.ini");
            }
        }
        //nahodíme view, abychom do něj mohli posílat případné errormessage a stringtabley
        $view = Zend_Layout::getMvcInstance()->getView();
        if (isset($this->aErrormessages)) {
            $view->errormessage = $this->aErrormessages;
        }

        $view->lang = $this->session->lang;
        $this->setStrings();
    }

    public function setStrings($newlang = null) {
        if (isset($newlang)) {
            $lang = $newlang;
        } else {
            $lang = $this->session->lang;
        }
        $view = Zend_Layout::getMvcInstance()->getView();
        //stringtable
        if ($lang == 'en') {
            $view->nacepu = 'Currently on tap';
            $view->galerie = 'Photogallery';
            $view->mapa = 'MAP';
            $view->otviracka = 'Mon - Sun';
            $view->klub_pravidla = 'Členství klubu - pravidla';
            $view->klub_akce = 'Klubové akce';
            $view->klub_jine_clanky = 'Jiné články';

            $view->form_name = 'Name:';
            $view->form_surname = 'Surname:';
            $view->form_phone = 'Phone number:';
            $view->form_email = 'E-mail:';
            $view->form_address = 'Address:';
            $view->form_city = 'City:';
            $view->form_zip = 'Zip code:';
            $view->form_country = 'Country:';
            $view->form_delivery = 'Delivery:';
            $view->form_payment = 'Payment:';
            //cena            
            $view->str_price0 = "Price: ";
            $view->str_price1 = "price without VAT: ";
            $view->str_price2 = "price including VAT: ";
            $view->str_price3 = "price (per item): ";
            $view->str_price4 = "Price per meter: ";
            $view->str_price5 = "Price per 0,5 meter: ";
            $view->str_price6 = "Price per m&sup2;: ";
            //objednávka
            $view->str_ask = "Ask about this product";
            $view->str_custom = "Order custom size";
            $view->str_available = "Pcs available:";
            $view->str_pile = "Pile: ";
            $view->str_pile1 = "cm";
            $view->str_grams = "Weight/m&#178;: : ";
            $view->str_grams1 = " g/m&#178;";
            $view->str_size = "Size: ";
            $view->str_material = "Material:";
            $view->str_production = "Production:";
            $view->str_size = "Size:";
            $view->str_weight = "Weight:";
            $view->str_stock = "In stock:";
            $view->str_pcs = "pcs";
            $view->str_add = "Add to basket";
            $view->str_empty = "This category is empty";
            $view->str_question = "Ask about this product";
            $view->str_care = "Care";
            $view->str_coarse1 = "Mountain Man (course) – one feather - the&nbsp;Rhodopa blankets";
            $view->str_coarse2 = "Rustic (slightly course) – two feathers - the&nbsp;Perelika blankets";
            $view->str_coarse3 = "Gentle (soft) – three feathers - the&nbsp;Karandilla blankets";
            $view->str_coarse4 = "Downy (ultra soft) – four feathers - the Merino&nbsp;blankets";
            $view->str_bysoftness = "By softness";
            $view->str_softness_text = "Softness";
            $view->str_softness_desc = "The most of the blankets in this category have the softness marked below";
            $view->str_softness_1 = "Mouintain man";
            $view->str_softness_2 = "Rustic";
            $view->str_softness_3 = "Gentle";
            $view->str_softness_4 = "Downy";
            //objednávání
            $view->str_order = "Order";
            $view->str_order_id = "Order ID:";
            $view->str_order_status = "Order status:";
            $view->str_order_timestamp = "Date of receipt:";
            $view->str_order_shipping_address = "Mailing address:";
            $view->str_order_step = "step";
            $view->str_order_continue = "Continue";
            $view->str_order_back = "Back";
            $view->str_order_empty_basket = "Empty basket";
            $view->str_order_fill_order = "Fill in form &#187;";
            $view->str_order_send = "Send form";
            $view->str_order_select = "Choose your pick-up location";
            $view->str_order_select_shipping = "Choose preferred shipping:";
            $view->str_order_select_payment = "Choose preferred payment:";
            $view->str_order_message = "Message for us (optional)";
            $view->str_order_product = "Product name";
            $view->str_order_size = "Size";
            $view->str_order_quantity = "Quantity";
            $view->str_order_unit_price = "Price per piece";
            $view->str_order_total_price = "Total price";
            $view->str_order_total = "Total ";
            $view->str_order_percent_of = "of";
            $view->str_order_details = "Order details ";
            $view->str_order_payment = "Payment details ";
            $view->str_order_VATincluded = "All prices include VAT.";
            $view->str_order_check = "You can check the status of your order here:";
            $view->str_order_link = "Show order status";
            $view->str_order_ready = "The order is ready for pickup";
            $view->str_order_sent = "Your order has been shipped";
            $view->str_order_subject = "We have recieved your order";
            $view->str_order_packing = "package";
            $view->str_order_paypal = "Pay via PayPal";
            $view->str_order_agree2 = "Souhlasím s obchodními podmínkami (viz. níže).";

            $view->menu_jidlo = 'Menu';
            $view->menu_nabidka = 'Lunch menu';
            $view->menu_obedy = 'Speacial of the day';
            $view->menu_menu = 'Complete menus';

            $view->menu_pivovice = 'Beer spirits';
            $view->menu_destilaty = 'Liquors';
            $view->menu_nealko = 'Soft drinks';
            $view->menu_ostatni = 'Other Beverages';

            $view->status_prijata = 'order accepted';
            $view->status_vrizeni = 'order is being processed';
            $view->status_odeslana = 'order shipped';
            $view->status_vyrizena = 'order payed for and finalised';
            $view->status_zrusena = 'order cancelled';

            //chyby v objednávacím formuláři
            $view->err_fill_order = "Please fill in your method of payment.";
            $view->err_fill_address1 = "You must select a shipping address.";
            $view->err_fill_shipping = "Please fill out your method of shipping.";
            $view->err_fill_name = "Please fill in your name.";
            $view->err_fill_surname = "Please fill in your surname.";
            $view->err_fill_phone = "Please fill in your phone number.";
            $view->err_fill_email = "Please fill in a valid email address.";
            $view->err_fill_address2 = "Please fill in your address.";
            $view->err_fill_city = "Please fill in your city.";
            $view->err_fill_zip = "Please fill in your zip code (numbers only).";
            $view->err_fill_country = "Please fill in your country.";
            $view->err_fill_agree = "You must agree with our terms and conditions to finalise your order.";

            $view->lang = 'en';

            $view->week = $array = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            //$view->month = $array = array("", "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $view->month = $array = array("", "JANUARY", "FEBUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");
        } else {
            $view->nacepu = 'Právě na čepu';
            $view->galerie = 'Fotogalerie';
            $view->mapa = 'MAPA';
            $view->otviracka = 'PO - NE';
            $view->klub_pravidla = 'Členství klubu - pravidla';
            $view->klub_akce = 'Klubové akce';
            $view->klub_jine_clanky = 'Jiné články';            

            $view->menu_jidlo = 'Stálý jídelní lístek';
            $view->menu_nabidka = 'Polední nabídka';
            $view->menu_obedy = 'Nabídka dne';
            $view->menu_menu = 'Nabídka menu';

            $view->menu_pivovice = 'Pivní pálenky';
            $view->menu_destilaty = 'Ovocné destiláty';
            $view->menu_nealko = 'Nealko';
            $view->menu_ostatni = 'Ostatní nápoje';

            $view->status_prijata = 'objednávka přijata';
            $view->status_vrizeni = 'objednávka se vyřizuje';
            $view->status_odeslana = 'zboží je expedováno';
            $view->status_vyrizena = 'objednávka vyřízena a zaplacena';
            $view->status_zrusena = 'objednávka zrušena';

            //cena
            $view->str_price0 = "cena: ";
            $view->str_price1 = "cena bez DPH: ";
            $view->str_price2 = "cena včetně DPH: ";
            $view->str_price3 = "cena (za jednotku): ";
            $view->str_price4 = "cena (za bm): ";
            $view->str_price5 = "0,5 bm: ";
            $view->str_price6 = "cena za m&sup2;: ";
            //objednávka  
            $view->str_order = "Objednávka";
            $view->str_order_id = "Identifikační číslo:";
            $view->str_order_status = "Stav objednávky:";
            $view->str_order_timestamp = "Datum přijetí:";
            $view->str_order_shipping_address = "Doručovací adresa:";
            $view->str_order_step = "krok";
            $view->str_order_continue = "Pokračovat";
            $view->str_order_back = "Zpět";
            $view->str_order_empty_basket = "Vyprázdnit košík";
            $view->str_order_fill_order = "Vyplnit objednávku";
            $view->str_order_send = "Odeslat objednávku";
            $view->str_order_select = "Vyberte místo odběru";
            $view->str_order_select_shipping = "Vyberte způsob dodání:";
            $view->str_order_select_payment = "Vyberte způsob platby:";
            $view->str_order_message = "Zpráva pro příjemce:";
            $view->str_order_product = "Název zboží";
            $view->str_order_size = "Velikost ";
            $view->str_order_quantity = "Množství";
            $view->str_order_unit_price = "Cena za ks";
            $view->str_order_unit_price_noVAT = "Cena (ks) <br/>(bez DPH)";
            $view->str_order_total_price = "Cena celkem ";
            $view->str_order_total_price_noVAT = "Cena celkem <br/>(bez DPH)";
            $view->str_order_percent_of = "z částky";
            $view->str_order_total = "Celkem ";
            $view->str_order_details = "Podrobnosti ";
            $view->str_order_payment = "Platba objednávky ";
            $view->str_order_VATincluded = "Všechny ceny jsou uvedené včetně DPH.";
            $view->str_order_check = "Stav objednávky můžete zkontrolovat zde:";
            $view->str_order_link = "Zobrazit objednávku";
            $view->str_order_ready = "Objednané zboží je připraveno k vyzvednutí";
            $view->str_order_sent = "Vaše objednávka byla předána přepravci";
            $view->str_order_subject = "Vaše objednávka byla uložena";
            $view->str_order_packing = "balíček";
            $view->str_order_paypal = "Zaplatit přes PayPal";
            $view->str_order_session = "Osobní informace byly vymazány z mezipaměti (session).";
            $view->str_order_free = 'ZDARMA';
            
            $view->str_picture = "obrázek";
            $view->str_product = "výrobek";
            $view->str_items = "počet";   
            $view->str_price = "cena";    
            $view->str_sum = "celkem";         
            $view->str_code = "kód produktu: ";
            $view->str_note = "poznámka: ";
            $view->str_size = "velikost: ";
            $view->str_size2 = "šířka: ";
            $view->str_weight = "hmotnost: ";
            $view->str_weight2 = "min. objednávka: ";
            $view->str_shipping1    = "Doprava: ";
            $view->str_shipping2    = "Způsob přepravy: ";            
            $view->str_payment1     = "Platba: ";            
            $view->str_payment2     = "Způsob platby: "; 
            $view->str_total = "Celkem: ";
            $view->str_empty = "Vysypat košík";
            $view->str_fill = "Vyplit údaje &#187;";
            $view->str_incomplete = "Formulář je nekompletní!";
            $view->str_required = "Než budeme moct objednávku přijmout, tak je třeba se přesvědčit zda jsou všechny údaje v pořádku.";
            $view->str_sofar = "Zatím máte košík prázdný!";
            $view->str_address = "Adresa";
            $view->str_shipping = "Doprava";
            $view->str_payment = "Platba";
            
            //formuláře
            $view->form_billing             = "Fakturační údaje";
            $view->form_name                = "Jméno: ";
            $view->form_surname             = "Příjmení: ";
            $view->form_phone               = "Telefon: ";
            $view->form_email               = "E-mail: ";
            $view->form_address             = "Ulice a č. p.: ";
            $view->form_city                = "Město: ";
            $view->form_zip                 = "PSČ: ";
            $view->form_country             = "Země: ";
            $view->form_delivery            = 'Způsob doručení:';
            $view->form_payment             = 'Způsob platby:';            
            $view->form_available1          = 'Bohužel máme jen';
            $view->form_available2          = 'výrobků k dispozici.';
            $view->form_shipping_address    = "Doručovací adresa:";
            $view->form_fill_shipping       = "Vyplňte prosím jinou dodací adresu.";
            $view->form_order_company       = "Faktura bude vystavena na firmu";
            $view->form_order_company_name  = "Jméno firmy:";
            $view->form_order_company_id    = "IČO:";
            $view->form_order_diff          = "Dodací údaje se liší od fakturačních údajů";
            $view->form_back                = "Zpět";
            $view->form_select_shipping     = "Vybrat dopravu";
            $view->form_select_payment      = "Vybrat platbu";
            $view->form_select_summary      = "Shrnutí";
            $view->form_order_sendmail      = "Přeji si zasílat novinky na email.";            
            $view->form_news                = 'Přeji si zasílat novinky na email.';
            $view->form_order_adult         = "Prohlašuji, že mi již bylo 18 let.";
            $view->form_summary             = "Závěrečná kontrola údajů"; 
            $view->form_send                = "Odeslat objednávku";
            $view->form_com = 'Faktura bude vystavena na firmu';
            $view->form_com_name = 'Jméno firmy:';
            $view->form_com_id = 'IČO:';
            $view->form_address2 = 'Doručovací adresa (uveďte pouze pokud se liší od adresy firmy):';
            $view->form_city2 = 'Město:';
            $view->form_zip2 = 'PSČ:';
            $view->form_address3 = 'Fakturační adresa:';
            $view->form_city3 = 'Město:';
            $view->form_zip3 = 'PSČ:';
            $view->form_country = 'Země:';
            $view->form_agree = 'Souhlasím s obchodními podmínkami (viz. níže).';
            $view->form_adult = 'Prohlašuji, že mi již bylo 18 let.';
            $view->form_diff = 'Dodací údaje se liší od fakturačních údajů';
            $view->form_diff_add = 'Vyplňte prosím jinou dodací adresu.';
            
            $view->str_billing_info = "Fakturační údaje";
            $view->str_delivery_data = "Dodací údaje";
            $view->str_czpost = "Českou poštou";
            $view->str_depost = "Deutsche Post";
            $view->str_payment = "Způsob platby:";
            $view->str_method1 = "Hotově při předání. (dobírka)";
            $view->str_method2 = "Kreditní kartou či paypal účtem. (předem)";
            $view->str_message = "Zpráva pro nás (volitelně):";
            $view->str_agree = "Souhlasím s <a onclick='' href='#podminky'>prodejními podmínkami</a>";
            $view->str_agree2 = "Souhlasím s obchodními podmínkami (viz. níže).";
            $view->str_mailing = "Přeji si zasílat informace o novinkách:";
            $view->str_confirm = "Potvrdit objednávku &#187;";
            $view->str_back = "&#171; Zpět na formulář";
            $view->str_ordered = "Objednali jste následující zboží:";
            $view->str_pay = "Zaplatit &#187;";
            $view->str_print = "Vytisknout";
            $view->str_check = "Nyní prosím klikněte na tlačítko zaplatit pro zaplacení Vaší objednávky.";
            $view->str_thanks = "Děkujeme!";
            $view->str_thanks2 = "Děkujeme za váš nákup!";
            $view->str_conditions = "Prodejní podmínky";
            $view->str_received = "Objednávka přijata";
            $view->str_sent = "Objednávka expedována";
            //faktura
            $view->str_invoice = "Faktura č. ";
            $view->str_supplier = "Dodavatel";
            $view->str_buyer = "Odběratel";
            $view->str_company = "Firma:";
            $view->str_ico = "IČ:";
            $view->str_dic = "DIČ";
            $view->str_listed = "zapsaná v obchodním rejstříku vedeném Městským soudem v Praze, oddíl C, vložka 148378";
            $view->str_date1 = "Datum vystavení: ";
            $view->str_date2 = "Datum zdanit. plnění: ";
            $view->str_date3 = "Splatnost: ";
            $view->str_paytype = "Způsob platby: ";
            $view->str_transfer1 = "před dodáním bezhotovostně";
            $view->str_transfer2 = "převodem";
            $view->str_transport = "Doprava: ";
            $view->str_paypal = "Paypal účet: ";
            $view->str_orderID = "Číslo objednávky.:";
            $view->str_shipping = "Doprava ";
            $view->str_vat1 = "DPH";
            $view->str_vat2 = "DPH %";
            $view->str_store = "Adresa prodejny: ";
            $view->str_final = "Celkem uhrazeno: ";
            $view->str_final_cashondelivery = "Hrazeno dobírkou: ";
            $view->str_account_sum = "Částka: ";
            $view->str_account_no = "Číslo účtu: ";
            $view->str_account_iban = "IBAN: ";
            $view->str_account_bankId = "Kód banky: ";
            $view->str_account_var = "Variabilní symbol: ";
            $view->str_account_swift = "SWIFT: ";
            $view->str_account_rate = "Kurz: ";
            //chyby v objednávacím formuláři
            $view->err_fill_order = "Prosím vyplňte způsob platby za objednávku.";
            $view->err_fill_address1 = "Musíte vybrat doručovací adresu.";
            $view->err_fill_shipping = "Prosím vyplňte způsob doručení zboží.";
            $view->err_fill_name = "Prosím vyplňte své jméno.";
            $view->err_fill_diff_name = "Prosím vyplňte své jméno.";
            $view->err_fill_surname = "Prosím vyplňte své přijmení.";
            $view->err_fill_diff_surname = "Prosím vyplňte své přijmení.";
            $view->err_fill_phone = "Prosím vyplňte své telefonní číslo.";
            $view->err_fill_email = "Prosím vyplňte validní email adresu.";
            $view->err_fill_address2 = "Prosím vyplňte svou adresu.";
            $view->err_fill_diff_address = "Prosím vyplňte svou adresu.";
            $view->err_fill_city = "Prosím vyplňte město.";
            $view->err_fill_diff_city = "Prosím vyplňte město.";
            $view->err_fill_zip = "Prosím vyplňte PSČ (pouze číselné hodnoty).";
            $view->err_fill_diff_zip = "Prosím vyplňte PSČ (pouze číselné hodnoty).";
            $view->err_fill_company_name = "Prosím vyplňte jméno firmy.";
            $view->err_fill_company_id = "Prosím vyplňte IČO firmy.";
            $view->err_fill_country = "Prosím vyplňte Zemi doručení.";
            $view->err_fill_agree = "Musíte souhlasit s obchodními podmínkami.";

            $view->week = $array = array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            //$view->month = $array = array("", "January", "Febuary", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December");
            $view->month = $array = array("", "JANUARY", "FEBUARY", "MARCH", "APRIL", "MAY", "JUNE", "JULY", "AUGUST", "SEPTEMBER", "OCTOBER", "NOVEMBER", "DECEMBER");

            //načteme si všechny důležité texty na všechny stránky z databáze
            $modelTexts = new Model_DbTable_Texts();
            $texts = $modelTexts->fetchTexts();
            $view->texts = $texts;
        }
    }

}
